
<style>
	.paypal_pagar_buttom{
		width: 110px;
		height: auto;

	}

	.datePicker{
		width: 220px;
		font-size: var(--tl);
	}

	.datePicker{
		background: white;
	}

	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
		background-color: white;
	}

	.form_datetime{
		height: 34px;
	}

	.form_datetime img{
		position: absolute;
		height: 30px;
		width: auto;
		top: 2px;
		left: 195px;
		z-index: 20;
	}

	#form-pagar{
		display: inline-block;
		border: 2px dashed var(--colorSeguir);
		border-radius: 10px;
		padding: 21px;
		padding-bottom: 15px;
	}

	#form-pagar > h2{
		margin: 0;
		position: absolute;
		background: white;
		font-size: var(--tl); 
		top: -12px; 
		padding: 0 6px;
	}

	.texto-agua{
		font-size: var(--txn);
		width: 161px;
		text-align: center;
	}

	.no-link, .no-link:link , .no-link:hover{
		text-decoration: none;
	}

</style>



	<div class="" style="text-align: center;">
		<h3>Elija la fecha y hora aproximada de su pedido</h3>
	</div>


<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8 center-block">
		<div class="input-append date form_datetime" data-date="2012-12-21T15:25:00Z">
		    <input class="form-control datePicker" size="16" type="text" value="" readonly>
		    <span class="add-on"><img src="{{asset('img/calendario.svg')}}" alt=""><i class="icon-th"></i></span>
		</div>
	</div>
</div>


 
   

<div class="row">
	<div class="col-lg-6 col-md-8 col-sm-7 col-xs-8 center-block">
		<div class="row ">
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 center-block mt-txl">

				@guest
					{!! Form::open([ 'url' => 'orders/create' , 'method' => 'GET' , 'id' => 'form-pagar' , 'style' => 'padding: 15px 25px']) !!}
						<h2 style="">Agendar</h2>
						{{ Form::submit('Inicia cuenta' , [ 'class' => 'btn btn-success']) }}
						<input type="hidden" name="fecha_reserva" class="fecha_reserva">
					{!! Form::close() !!}

				<div class="texto-agua">¡Importante!: necesitamos saber quien realiza la orden</div>
				@else

					{!! Form::open([ 'url' => 'orders/create' , 'method' => 'GET' , 'id' => 'form-pagar' , 'style' => 'padding: 15px 25px']) !!}
						<h2 style="">Agendar</h2>
						{{ Form::submit('Ordena ya!' , [ 'class' => 'btn btn-success']) }}
						<input type="hidden" name="fecha_reserva" class="fecha_reserva">
					{!! Form::close() !!}
				@endguest

				


			</div>


			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 center-block mt-txl">
				{!! Form::open([ 'url' => '/carrito' , 'method' => 'POST' , 'id' => 'form-pagar']) !!}
					<h2>Pagar</h2>
					<input type="image" class="paypal_pagar_buttom" value="Pagar" src="{{asset('img/PayPal.png')}}" />
					<input type="hidden" name="fecha_reserva" class="fecha_reserva">
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
    
   	

    

     $(document).ready(function(){


     	
     	var datePicker = {
     		datapicker: document.querySelector('.datePicker'),
     		imputHidden: document.querySelectorAll('.fecha_reserva'),
     		dia: null,
     		mes: null,
     		año: null,
     		hora: null,
     		min: null,
     		saltoMin: 15,
     		datePickerHoy: function(){

     			this.setDate();

	 			var fString = this.agregarCero(this.dia) + "/" + this.agregarCero(this.mes) + "/" + this.año + " - " + this.agregarCero(this.hora) + ":" + this.agregarCero(this.min);

	    		datePicker.datapicker.value = fString;
	    		
	    		for( var i = 0 ; i < datePicker.imputHidden.length ; i++){
	    			datePicker.imputHidden[i].value = fString;
	    		}

	   		},
	   		setDate: () => {
	   			var fecha = new Date();

	   			if(fecha.getHours() > 10 && fecha.getHours() < 22 ){
	   				var f = datePicker.sumarMin(fecha,datePicker.minMul(fecha.getMinutes()) - fecha.getMinutes());   
	   			}else if(fecha.getHours() >= 22){
	   				var f = datePicker.sumarDia(fecha);
	   			}else {
	   				fecha.setHours(10);
	   				fecha.setMinutes("0");
	   				var f = fecha;
	   			}

	   			datePicker.dia = f.getDate();
	   			datePicker.mes = f.getMonth()+1;
	   			datePicker.año = f.getFullYear();
	   			datePicker.hora = f.getHours();
	   			datePicker.min = f.getMinutes();
	   		},
	   		agregarCero: (n)=>{
	   			if(n < 10){
	   				return "0"+ n;
	   			}

	   			return n;
	   		},
	   		minMul:(m)=>{
	   			//00 15 30 45 59
	   			var saltoMin = datePicker.saltoMin;

	   			for(var i = 2 ; i < 30; i++){
	   				if(m <= saltoMin){
	   					return saltoMin;
	   				}

	   				saltoMin = datePicker.saltoMin*i;
	   			}

	   			return 0;
	   		},
	   		sumarMin: (f,m)=>{
	   			f.setMinutes(f.getMinutes()+m);

	   			return f;
	   		},
	   		sumarDia: (f)=>{
	   			f.setDate(f.getDate() + 1);
	   			f.setHours(10);
	   			return f;
	   		}
     	}

     		datePicker.datePickerHoy();
     	 

       $(".form_datetime").datetimepicker({
	        language: 'es',
	        format: "dd/mm/yyyy - hh:ii ",
	        showMeridian: false,
	        autoclose: true,
	        //todayBtn: true,
	        weekStart: 1,
	        startDate: new Date(),
	        daysOfWeekDisabled: [0],
	        todayHighlight: true,
	        minuteStep: 15,
	        hoursDisabled: [0,1,2,3,4,5,6,7,8,9,22,23],

	    }).on('changeDate', function(ev){
       		for( var i = 0 ; i < datePicker.imputHidden.length ; i++){
    			datePicker.imputHidden[i].value = datePicker.datapicker.value;
    		}
       });
    });
</script> 