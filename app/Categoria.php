<?php

namespace StopTime;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categoria'; //nombre de la tabla

    protected $primaryKey = 'id'; //primarykay campo 

    public $timestamps = true; // si queremos los dos campos extras de fecha de modificacion y creacion

    protected $fillable = [ //campos que agremamos al modelo
        'nombre',
        'descripcion',
        'estado'
    ];

    protected $guarded = [ //campos que no agremamos al modelo
        
    ];

    //relaciones
    public function articulos(){
        return $this->hasMany('app/Articulo' , 'id_categoria');
    }

    //general

    public static function forMenu(){
        return Categoria::stateOn();
    }


    //scope

    public function scopeStateOn($query){
        return $query->where('estado', '=', 1);
    }
}
