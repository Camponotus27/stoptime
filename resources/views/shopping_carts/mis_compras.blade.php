@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
		{!! link_to(url('/'), '', ['class' => 'btn-atras']) !!}
		<h3> 
			Mis Ordenes
		</h3>
	</div>
</div>

<div class="table-resp-cont">
	<table class="table-resp" style="min-width: 700px">
		<thead>
			<th>ID</th>
			<th>FECHA RESERVA</th>
			<th>MONTO</th>
			<th>PAGADO</th>
			<th>OPCIONES</th>
		</thead>
		@foreach($orders as $order)
		<tr>
			<td>{{ $order->id }}</td>
			<td>{{ Carbon\Carbon::parse($order->shopping()->first()->fecha_reserva)->format('d-m-Y H:i')  }}</td>
			<td>${{ $order->shopping()->first()->total() }}</td>
			<td>{{($order->pagado == 1)? "Pagado":""}}</td>
			<td>
				<a class="btn btn-info" href="{{url('/compras/'.$order->shopping()->first()->customid)}}">Ver</a>
			</td>

		</tr>
		@endforeach
	</table>
</div>
@endsection