@extends ('layouts.admin')
@section ('contenido')

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
		{!! link_to(URL::previous(), '', ['class' => 'btn-atras']) !!}
		<h3> 
			Tu carrito de compras
		</h3>
	</div>
</div>

<div class="table-resp-cont">
	<table class="table-resp" style="min-width: 700px">
		<thead>
			<th>Nombre</th>
			<th>Imagen</th>
			<th>Precio</th>
			<th>INSUMOS</th>
			<th>CREMA</th>
			<th>AZUCAR</th>
			<th>SERVIR</th>
			<th>Acciones</th>
		</thead>
		@foreach($articulos as $art)
		<tr class="tr-shoppingcart-eliminar-{{$art->id}}">
			<td>{{ $art->nombre}}</td>
			<td class="imagen-tabla">
					<img src="{{asset('imagenes/articulos/'.$art->imagen)}}" alt="{{$art->imagen}}" class="img-thumbnail">
			</td>
			<td>${{ $art->precio}}</td>
			<td>{{ $art->pivot->insumos}}</td>
			<td>{{ $art->pivot->crema}}</td>
			<td>{{ $art->pivot->azucar}}</td>
			<td>{{ $art->pivot->servir}}</td>
			<td>

				@include( 'in_shopping_carts.delete' , ['articulo' => $art->id ] )

			</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="1" ></td>
			<td> <h2>Total</h2></td>
			<td><h2>${{$total}}</h2></td>
		</tr>
	</table>
</div>

@include('shopping_carts.form')
@endsection