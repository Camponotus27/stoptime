<?php

use StopTime\Articulo;

namespace StopTime;

class PayPal 
{
	
	private $_apiContext;
	private $shopping_cart;
	private $_ClientId = 'Abg5LywWYsNCnuwVc9Jz9-d3LVhRG_pZ0_Hac35aNDW-mrUpV2-h2fCDP3dNBkaaca-zS4ayEi_hBwKw';
	private $_ClientSecret = 'EEDLtlMsha5-CJc_8_SESDsnS1qFewOJ3JMOkSgg-lGFU9b_ek-PbkTxC6iZPFJFbbfIL7wXDd5DvykW';

	public function __construct($shopping_cart)
	{
		$this->_apiContext = \PaypalPayment::ApiContext($this->_ClientId , $this->_ClientSecret);

		$config = config("paypal_payment");
		$flatConfig = array_dot($config);

		$this->_apiContext->setConfig($flatConfig);

		$this->shopping_cart = $shopping_cart;
	}

	public function generate(){


		$payment = \PaypalPayment::payment()->setIntent('sale')
									->setPayer($this->payer())
									->setTransactions([$this->transaction()])
									->setRedirectUrls($this->redirectURL());

		try{
			$payment->create($this->_apiContext);
		}catch(\Exception $ex){
			dd($ex);
			exit(1);
		}

		return $payment; 
	}

	public function payer(){
		return \PaypalPayment::payer()->setPaymentMethod('paypal'); //Aca se colocaria tarjeta y los datos de esta
	}

	public function transaction(){
		return \PaypalPayment::transaction()
							->setAmount($this->amount())
							->setItemList($this->items())
							->setDescription('Compra en StopTime')
							->setInvoiceNumber(uniqid());
	}

	public function amount(){

		return \PaypalPayment::amount()
							->setCurrency('USD')
							->setTotal($this->total());
	}

	public function redirectURL(){
		$baseURL = url('/');
		return \PaypalPayment::redirectUrls()
							->setReturnUrl($baseURL.'/payments/store')
							->setCancelUrl($baseURL.'/carrito');	
	}

	public function total(){
		$articulos = $this->shopping_cart->articulos()->get();

		$suma = 0;

		foreach ($articulos as $articulo) {
			$suma += $articulo->precioUS();
		}

		return $suma;
	}

	public function items(){
		$items = [];

		$articulos = $this->shopping_cart->articulos()->get();

		foreach ($articulos as $articulo) {
			array_push($items, $articulo->paypalItem());
		}

		return \PaypalPayment::itemList()->setItems($items);
	}


	public function execute($paymentId,$payerId){
		$payment = \PaypalPayment::getById($paymentId , $this->_apiContext);

		$execution = \PaypalPayment::PaymentExecution()
							->setPayerId($payerId);

		return $payment->execute($execution , $this->_apiContext);
 
	}
}