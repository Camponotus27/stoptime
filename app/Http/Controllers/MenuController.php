<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;

use StopTime\Http\Requests;


use StopTime\Articulo;
use StopTime\Categoria;
use StopTime\Insumo;
use Illuminate\Support\Facades\Redirect;
use StopTime\Http\Requests\ArticuloFormRequest;

class MenuController extends Controller
{
    public function __construct(){
    	
    }
    
    public function index(Request $request){
        $articulos = Articulo::where('estado'  , '=', '1')
                        ->paginate(10);

        return view('menu.index', ["articulos"=>$articulos]);

    	$query = trim($request->get('searchText'));

        $categoria = Categoria::forMenu()->get();

    	return view('menu.index', ["categorias"=> $categoria, "searchText"=>$query]);
    }

    public function create(){
    	
    }

    public function store(ArticuloFormRequest $request){
    		
    }
    public function show($id){
    	
    }
    public function edit($id){
    	
    }
    public function update(ArticuloFormRequest $request, $id){
    	
    }
    public function destroy($id){
    	
    }

}