@extends ('layouts.admin')
@section ('contenido')

<style>
	
	.titulo-form{
		font-size: var(--txxl);
		text-align: center;
	}

	.creado{
		color: #13D806;
		text-transform: capitalize;
	}

	.Procesada{
		color: #FAD406;
		text-transform: capitalize;
	}

</style>

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block">
		{!! link_to(url('/compras'), '', ['class' => 'btn-atras']) !!}
		<h3> 
			Mis Ordenes
		</h3>
	</div>
</div>

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 center-block">
		<h3 class="titulo-form"> 
			Tu orden se realizo con exito!
		</h3>
		<h4>Estado : <span class="{{(!$order->status)? 'Procesada' : $order->status}}">  {{(!$order->status)? 'Procesada' : $order->status}} </span></h4>
		<p>Corrobora tus detalles del pedido</p>
	</div>
</div>

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 center-block">
		<div class="form-group">
			<label for="">Nombre</label>
			<div class="form-text">
				{{$order->recipient_name}}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 center-block">
		<div class="form-group">
			<label for="">Correo</label>
			<div class="form-text">
				{{$order->email}}
			</div>
		</div>
	</div>
</div>


<div class="table-resp-cont">
	<table class="table-resp" style="min-width: 700px">
		<thead>
			<th>Nombre</th>
			<th>Imagen</th>
			<th>Precio</th>
			<th>INSUMOS</th>
			<th>CREMA</th>
			<th>AZUCAR</th>
			<th>SERVIR</th>
		</thead>
		@foreach($articulos as $art)
		<tr class="tr-shoppingcart-eliminar-{{$art->id}}">
			<td>{{ $art->nombre}}</td>
			<td class="imagen-tabla">
					<img src="{{asset('imagenes/articulos/'.$art->imagen)}}" alt="{{$art->imagen}}" class="img-thumbnail">
			</td>
			<td>${{ $art->precio}}</td>
			<td>{{ $art->pivot->insumos}}</td>
			<td>{{ $art->pivot->crema}}</td>
			<td>{{ $art->pivot->azucar}}</td>
			<td>{{ $art->pivot->servir}}</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="1" ></td>
			<td> <h2>Total</h2></td>
			<td><h2>${{$total}}</h2></td>
		</tr>
	</table>
</div>




	

<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 center-block">
		{{-- <a class="btn btn-success" href="{{url('/compras/'.$shopping_cart->customid)}}">Ir a mis pedidos</a> --}}

		<a class="btn btn-success" href="{{url('/compras')}}">Ir a mis pedidos</a>
		
		<a class="btn btn-success" href="{{url('/')}}">Home</a>
	</div>
</div>

@endsection