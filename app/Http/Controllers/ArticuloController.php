<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;

use StopTime\Http\Requests;

use StopTime\Articulo;
use StopTime\Categoria;
use StopTime\Insumo;
use Illuminate\Support\Facades\Redirect;
use StopTime\Http\Requests\ArticuloFormRequest;
use DB;

class ArticuloController extends Controller
{
    public function __construct(){
    	
    }
    
    public function index(Request $request){
    	$query = trim($request->get('searchText'));
        $articulos = Articulo::where('estado'  , '=', '1')
                        ->paginate(7);

        $insumos = Insumo::orderNombre()->get();

        $cant_insumos = $insumos->count();

    	return view('almacen.articulo.index', ["articulos"=>$articulos, "searchText"=>$query , "cant_insumos"=> $cant_insumos]);
    }

    public function create(){
        $insumos = Insumo::orderNombre()->get();
        $categorias = Categoria::stateOn()->get();

    	return view("almacen.articulo.create", compact('insumos' , 'categorias'));
    }
    
    public function store(ArticuloFormRequest $request){
    	$articulo = new Articulo;
        if(!empty($request->get('cant_insumos')))
    	   {$articulo->cant_insumos = $request->get('cant_insumos');}
        $articulo->id_categoria = $request -> get('id_categoria');
        $articulo->nombre = $request -> get('nombre');
        $articulo->crema = $request -> get('crema');
    	$articulo->azucar = $request -> get('azucar');
    	$articulo->descripcion = $request -> get('descripcion');
    	$articulo->stock = $request -> get('stock');
        $articulo->precio = $request -> get('precio');
    	$articulo->estado = '1'; 

        if(!empty($request ->get('crema'))){
             $articulo -> crema = 1;
        }else{
             $articulo -> crema = 0;
        }

        if(!empty($request ->get('azucar'))){
            $articulo -> azucar = 1;
        }else{
             $articulo -> azucar = 0;
        }


        Articulo::storageImagen($request , $articulo);

    	$articulo -> save();

        $articulo->insumos()->sync($request->get('insumos'));

    	return Redirect::to('almacen/articulo')->with('info', 'Articulo guardado con éxito');
    }

    public function show($id){
    	return view('almacen.articulo.show', ["articulo"=>Articulo::findOrFail($id)]);
    }

    public function edit(Articulo $articulo){

        $insumos = Articulo::chekedInsumos($articulo);
        $categorias = Categoria::stateOn()->get();

    	return view('almacen.articulo.edit', compact('articulo' , 'insumos' , 'categorias'));
    }

    public function update(ArticuloFormRequest $request, $id){

    	$articulo = Articulo::findOrFail($id);
        $articulo->id_categoria = $request -> get('id_categoria');
    	$articulo->cant_insumos = $request->get('cant_insumos');
    	$articulo->nombre = $request ->get('nombre');
        $articulo->crema = $request -> get('crema');
        $articulo->azucar = $request -> get('azucar');
    	$articulo->descripcion = $request ->get('descripcion');
    	$articulo->stock = $request ->get('stock');
        $articulo->precio = $request ->get('precio');

        if(!empty($request ->get('crema'))){
             $articulo -> crema = 1;
        }else{
             $articulo -> crema = 0;
        }

        if(!empty($request ->get('azucar'))){
            $articulo -> azucar = 1;
        }else{
             $articulo -> azucar = 0;
        }

        Articulo::storageImagen($request , $articulo);   

    	$articulo -> update();

        $articulo->insumos()->sync($request->get('insumos'));

    	return Redirect::to('almacen/articulo')->with('info', 'Articulo actualizado con éxito');;
    }
    public function destroy($id){
    	$articulo = Articulo::findOrFail($id);
    	$articulo -> estado = "0";
    	$articulo -> update();
    	return Redirect::to('almacen/articulo');
    }

}
