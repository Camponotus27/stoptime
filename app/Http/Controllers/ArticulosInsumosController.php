<?php

namespace StopTime\Http\Controllers;

use StopTime\ArticulosInsumos;
use Illuminate\Http\Request;

class ArticulosInsumosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \StopTime\ArticulosInsumos  $articulosInsumos
     * @return \Illuminate\Http\Response
     */
    public function show(ArticulosInsumos $articulosInsumos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \StopTime\ArticulosInsumos  $articulosInsumos
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticulosInsumos $articulosInsumos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \StopTime\ArticulosInsumos  $articulosInsumos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticulosInsumos $articulosInsumos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StopTime\ArticulosInsumos  $articulosInsumos
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticulosInsumos $articulosInsumos)
    {
        //
    }
}
