<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArticuloTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('articulo', function(Blueprint $table)
		{
			$table->foreign('id_categoria', 'articulo_ibfk_1')->references('id')->on('categoria')->onUpdate('CASCADE')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articulo', function(Blueprint $table)
		{
			$table->dropForeign('articulo_ibfk_1');
		});
	}

}
