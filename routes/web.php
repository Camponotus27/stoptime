<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MenuNavegacionController@index')->name('menu');

//Route::resource('almacen/categoria','CategoriaController');
//Route::resource('almacen/articulo','ArticuloController');

Route::resource('in_shopping_carts','InShoppingCartsController', [
	'only' => ['store' , 'destroy']
]);
Route::resource('compras','ShoppingCartsController', [
	'only' => ['show']
]);


/*Route::resource('orders' , 'OrdersController', [
	'only' => ['index' , 'store' ,'update' , 'show' , 'destroy']
]);*/

//Route::get('/orderDay' , 'OrdersController@orderDay');


Route::resource('insumos' , 'InsumoController');

Route::get('/carrito' , 'ShoppingCartsController@index');

Route::post('/carrito' , 'ShoppingCartsController@checkout');
Route::get('/payments/store' , 'PaymentsController@store');

/*
Gives you these named routes:

Verb    Path                        Action  Route Name
GET     /users                      index   users.index
GET     /users/create               create  users.create
POST    /users                      store   users.store
GET     /users/{user}               show    users.show
GET     /users/{user}/edit          edit    users.edit
PUT     /users/{user}               update  users.update
DELETE  /users/{user}               destroy users.destroy

*/

//->middleware('auth');

//Pruebas

Route::get('/variable/{var}', function($var){
	return "La variable es :".$var;
});

// autentificacion
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/{slug?}', 'MenuController@index');


	//menu pruebas

	Route::get('menu' , 'MenuController@index')
	->name('menu.index');

//Permisos y roles

Route::middleware(['auth'])->group(function(){

		//Roles
	Route::get('roles/create' , 'RoleController@create')->name('roles.create')
		->middleware('permission:roles.create');

	Route::post('roles' , 'RoleController@store')->name('roles.store')
		->middleware('permission:roles.store');

	Route::get('roles' , 'RoleController@index')->name('roles.index')
		->middleware('permission:roles.index');

	Route::get('roles/{role}' , 'RoleController@show')->name('roles.show')
		->middleware('permission:roles.show');

	Route::get('roles/{role}/edit' , 'RoleController@edit')->name('roles.edit')
		->middleware('permission:roles.edit');

	Route::put('roles/{role}' , 'RoleController@update')->name('roles.update')
		->middleware('permission:roles.update');

	Route::delete('roles/{role}' , 'RoleController@destroy')->name('roles.destroy')
		->middleware('permission:roles.destroy');

		//Articulo 
	Route::get('almacen/articulo/create' , 'ArticuloController@create')
		->name('articulo.create')
		->middleware('permission:almacen.articulo.create');

	Route::post('almacen/articulo' , 'ArticuloController@store')
		->name('articulo.store')
		->middleware('permission:almacen.articulo.store');

	Route::get('almacen/articulo' , 'ArticuloController@index')
		->name('articulo.index')
		->middleware('permission:almacen.articulo.index');

	Route::get('almacen/articulo/{articulo}' , 'ArticuloController@show')
		->name('articulo.show')
		->middleware('permission:almacen.articulo.show');

	Route::get('almacen/articulo/{articulo}/edit' , 'ArticuloController@edit')
		->name('articulo.edit')
		->middleware('permission:almacen.articulo.edit');

	Route::patch('almacen/articulo/{articulo}' , 'ArticuloController@update')
		->name('articulo.update')
		->middleware('permission:almacen.articulo.update');

	Route::delete('almacen/articulo/{articulo}' , 'ArticuloController@destroy')
		->name('articulo.destroy')
		->middleware('permission:almacen.articulo.destroy');


		//categoria 
	Route::get('almacen/categoria/create' , 'CategoriaController@create')
		->name('categoria.create')
		->middleware('permission:almacen.categoria.create');

	Route::post('almacen/categoria' , 'CategoriaController@store')
		->name('categoria.store')
		->middleware('permission:almacen.categoria.store');

	Route::get('almacen/categoria' , 'CategoriaController@index')
		->name('categoria.index')
		->middleware('permission:almacen.categoria.index');

	Route::get('almacen/categoria/{categoria}' , 'CategoriaController@show')
		->name('categoria.show')
		->middleware('permission:almacen.categoria.show');

	Route::get('almacen/categoria/{categoria}/edit' , 'CategoriaController@edit')
		->name('categoria.edit')
		->middleware('permission:almacen.categoria.edit');

	Route::patch('almacen/categoria/{categoria}' , 'CategoriaController@update')
		->name('categoria.update')
		->middleware('permission:almacen.categoria.update');

	Route::delete('almacen/categoria/{categoria}' , 'CategoriaController@destroy')
		->name('categoria.destroy')
		->middleware('permission:almacen.categoria.destroy');

		//Users
	Route::get('users' , 'UserController@index')
		->name('users.index')
		->middleware('permission:users.index');

	Route::get('users/{user}' , 'UserController@show')
		->name('users.show')
		->middleware('permission:users.show');

	Route::get('users/{user}/edit' , 'UserController@edit')
		->name('users.edit')
		->middleware('permission:users.edit');

	Route::put('users/{user}' , 'UserController@update')
		->name('users.update')
		->middleware('permission:users.update');

	Route::delete('users/{user}' , 'UserController@destroy')
		->name('users.destroy')
		->middleware('permission:users.destroy');


			//Orders
	Route::get('orders/create' , 'OrdersController@create')->name('orders.create')
		->middleware('permission:orders.create');

	Route::post('orders' , 'OrdersController@store')->name('orders.store')
		->middleware('permission:orders.store');

	Route::get('orders' , 'OrdersController@index')->name('orders.index')
		->middleware('permission:orders.index');

	Route::get('orders/{order}' , 'OrdersController@show')->name('orders.show')
		->middleware('permission:orders.show');

	Route::put('orders/{order}' , 'OrdersController@update')->name('orders.update')
		->middleware('permission:orders.update');

	Route::delete('orders/{order}' , 'OrdersController@destroy')->name('orders.destroy')
		->middleware('permission:orders.destroy');

	Route::get('/orderDay' , 'OrdersController@orderDay')->name('ordersDay.index')
		->middleware('permission:ordersDay.index');


			//Compras

		Route::get('/compras' , 'ShoppingCartsController@misCompras')->name('misCompras.index')
		->middleware('permission:misCompras.index');

});