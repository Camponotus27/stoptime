<?php

namespace StopTime;

use Illuminate\Database\Eloquent\Model;

class ArticulosInsumos extends Model
{
    protected $fillable = [
    	'articulo_id',
    	'insumo_id',
    	'cant_porcion'
    ];
}
