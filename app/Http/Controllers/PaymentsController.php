<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;
use StopTime\ShoppingCart;
use StopTime\PayPal;
use StopTime\Order;
use Auth;

class PaymentsController extends Controller
{
    public function __construct(){
         $this->middleware('shoppingcart');
    }

    public function store(Request $request){

        //xdd($request);

    	$shopping_cart = $request->shopping_cart;

        $paypal = new Paypal($shopping_cart);
        $response = $paypal->execute($request->paymentId , $request->PayerID);

        if($response->state == 'approved'){
            \Session::remove('shopping_cart_id');
            if(Auth::check()){
                $order = Order::createFromReserva($shopping_cart , null ,1);
            }else{
                $order = Order::createFromPayPalResponse($response , $shopping_cart);
            }
        	
            $shopping_cart->approve();
            $articulos = $shopping_cart->articulos()->get();
            $total = $shopping_cart->total();
        }else{
            return view('principal')->with('Algo no salio bien, informe de este suceso');
        }



  		return view('shopping_carts.completed' , compact('shopping_cart' , 'order' , 'articulos' , 'total'));
    }
}
