<?php

namespace StopTime\Http\Controllers;

use Illuminate\Http\Request;




use StopTime\Categoria;
use Illuminate\Support\Facades\Redirect;
use StopTime\Http\Requests\CategoriaFormRequest;
use DB;

class CategoriaController extends Controller
{
	public function __construct(){
    	
    }
    public function index(Request $request){
    	$query = trim($request->get('searchText'));
    	$categorias = Categoria::stateOn()->get();
    	return view('almacen.categoria.index', ["categorias"=>$categorias, "searchText"=>$query]);
    }
    public function create(){
    	return view("almacen.categoria.create")    ;
    }
    public function store(CategoriaFormRequest $request){

        /* store(Request $request) 
        $categoria = Categoria::create($request -> all());

        return redirect() -> route('categoria.edit',$categoria->id)->with('info', 'Hola que hace'); */

    	$categoria = new Categoria;
    	$categoria -> nombre = $request ->get('nombre');
    	$categoria -> descripcion = $request ->get('descripcion');
    	$categoria -> estado = '1';
    	$categoria -> save();

    	return Redirect::to('almacen/categoria')->with('info', 'Categoria guardado con éxito');;	

    }
    public function show(Categoria $categoria){
        // show($id)
        //$categoria = Categoria::findOrFail($id);
    	//return view('almacen.categoria.show', ["categoria"=>$categoria]);
        return view('almacen.categoria.show', compact('categoria'));

    }
    public function edit($id){
    	return view('almacen.categoria.edit', ["categoria"=>Categoria::findOrFail($id)]);
    }
    public function update(CategoriaFormRequest $request, Categoria $categoria){
    	$categoria -> update($request->all());
    	return Redirect::to('almacen/categoria')->with('info', 'Categoria actualizada con éxito');;


        /*
        update(CategoriaFormRequest $request, $id){
        $categoria = Categoria::findOrFail($id);
        $categoria -> nombre = $request -> get('nombre');
        $categoria -> descripcion = $request -> get('descripcion');
        $categoria -> update();
        return Redirect::to('almacen/categoria');*/
    }
    public function destroy($id){
    	$categoria = Categoria::findOrFail($id);
    	$categoria -> estado = "0";
    	$categoria -> update();
    	return Redirect::to('almacen/categoria');


        /*
        destroy(Categoria $categoria)

        $categoria->destroy();
        return back()->with('info','Eliminado');*/
    }
}
