<?php

namespace StopTime\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsumoFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        if(!empty($this->insumo->id)){
            return [
                'nombre' => 'required|max:50|unique:insumos,id,'.$this->insumo->id,
                'stock' => 'required|numeric',
                'descripcion' => 'max:500'
            ];
        }else{
             return [
                'nombre' => 'required|max:50|unique:insumos',
                'stock' => 'required|numeric',
                'descripcion' => 'max:500'
            ];
        }
    }
}
