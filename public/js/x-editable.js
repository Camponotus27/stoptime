
$.fn.editable.defaults.mode = 'inline';

$.fn.editable.defaults.ajaxOptions = {type: 'PUT'};

$(document).ready(function(){
	$('.set-guide_number').editable({
		emptytext: 'Vacio',
	});

	$('.select-status').editable({
		source:[
			{value: 'creado' , text: 'Creado'},
			{value: 'servido' , text: 'Servido'},
			{value: 'cancelado' , text: 'Cancelado'}
		],
		showbuttons: false
	});
});