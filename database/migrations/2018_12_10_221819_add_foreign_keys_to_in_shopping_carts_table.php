<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInShoppingCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('in_shopping_carts', function(Blueprint $table)
		{
			$table->foreign('shopping_cart_id', 'in_shopping_carts_ibfk_1')->references('id')->on('shopping')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('in_shopping_carts', function(Blueprint $table)
		{
			$table->dropForeign('in_shopping_carts_ibfk_1');
		});
	}

}
