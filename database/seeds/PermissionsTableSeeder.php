<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //ROl depeveria ir a parte pero ñe
       Role::create([
          'name'      => 'Admin',
          'slug'      => 'admin',
          'special'   => 'all-access',
        ]);

      //Opciones
        Permission::create([
          'name'      => 'Visualizar las opciones',
          'slug'      => 'opciones.index',
          'description'   => 'Lista todas las opciones',
        ]);


    	//user
        Permission::create([
        	'name'  		=> 'Navegar usuarios',
        	'slug'			=> 'users.index',
        	'description' 	=> 'Lista todos usuarios',
        ]);

         Permission::create([
        	'name'  		=> 'Ver detalle del usuario',
        	'slug'			=> 'users.show',
        	'description' 	=> 'Ver detalle de usuarios',
        ]);

          Permission::create([
        	'name'  		=> 'Editar usuario',
        	'slug'			=> 'users.edit',
        	'description' 	=> 'Editar informacion de un usuario',
        ]);

           Permission::create([
        	'name'  		=> 'Eliminar usuarios',
        	'slug'			=> 'users.destroy',
        	'description' 	=> 'Eliminar usuarios',
        ]);

           //roles
        Permission::create([
        	'name'  		=> 'Navegar roles',
        	'slug'			=> 'roles.index',
        	'description' 	=> 'Lista todos roles',
        ]);

         Permission::create([
        	'name'  		=> 'Ver detalle del rol',
        	'slug'			=> 'roles.show',
        	'description' 	=> 'Ver detalle de rol',
        ]);

          Permission::create([
        	'name'  		=> 'Editar rol',
        	'slug'			=> 'roles.edit',
        	'description' 	=> 'Editar informacion de un rol',
        ]);

           Permission::create([
        	'name'  		=> 'Crear rol',
        	'slug'			=> 'roles.create',
        	'description' 	=> 'Crear informacion de un rol',
        ]);

           Permission::create([
        	'name'  		=> 'Eliminar rol',
        	'slug'			=> 'roles.destroy',
        	'description' 	=> 'Eliminar rol',
        ]);

              //Articulos
        Permission::create([
        	'name'  		=> 'Navegar articulo',
        	'slug'			=> 'almacen.articulo.index',
        	'description' 	=> 'Lista todos articulo',
        ]);

         Permission::create([
        	'name'  		=> 'Ver detalle del articulo',
        	'slug'			=> 'almacen.articulo.show',
        	'description' 	=> 'Ver detalle de articulo',
        ]);

          Permission::create([
        	'name'  		=> 'Editar articulo',
        	'slug'			=> 'almacen.articulo.edit',
        	'description' 	=> 'Editar informacion de un articulo',
        ]);

           Permission::create([
        	'name'  		=> 'Crear articulo',
        	'slug'			=> 'almacen.articulo.create',
        	'description' 	=> 'Crear informacion de un articulo',
        ]);

           Permission::create([
        	'name'  		=> 'Eliminar articulo',
        	'slug'			=> 'almacen.articulo.destroy',
        	'description' 	=> 'Eliminar articulo',
        ]);

        //Ordenes
        Permission::create([
          'name'      => 'Navegar ordenes',
          'slug'      => 'orders.index',
          'description'   => 'Lista todas las ordenes',
        ]);

         Permission::create([
          'name'      => 'Ver detalle del orden',
          'slug'      => 'orders.show',
          'description'   => 'Ver detalle de la orden',
        ]);

          Permission::create([
          'name'      => 'Editar orden',
          'slug'      => 'orders.edit',
          'description'   => 'Editar informacion de la orden',
        ]);

           Permission::create([
          'name'      => 'Crear orden',
          'slug'      => 'orders.create',
          'description'   => 'Crear informacion de la orden',
        ]);

           Permission::create([
          'name'      => 'Eliminar orden',
          'slug'      => 'orders.destroy',
          'description'   => 'Eliminar orden',
        ]);

           Permission::create([
          'name'      => 'Ordenes del dia',
          'slug'      => 'ordersDay.index',
          'description'   => 'Muestra todas las ordenes realizadas para el dia actual',
        ]);

    }
}
