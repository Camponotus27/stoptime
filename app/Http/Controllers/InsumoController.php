<?php

namespace StopTime\Http\Controllers;

use StopTime\Insumo;
use Illuminate\Http\Request;
use StopTime\Http\Requests\InsumoFormRequest;

class InsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insumos = Insumo::paginate();

        return view( 'insumos.index' , compact('insumos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insumos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsumoFormRequest $request)
    {
        Insumo::create($request->all());

        return redirect()->route('insumos.index')
            ->with('info', 'Insumo guardado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \StopTime\Insumo  $insumo
     * @return \Illuminate\Http\Response
     */
    public function show(Insumo $insumo)
    {
        return "soy el show de $insumo->nombre";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \StopTime\Insumo  $insumo
     * @return \Illuminate\Http\Response
     */
    public function edit(Insumo $insumo)
    {
        return view('insumos.edit', compact('insumo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \StopTime\Insumo  $insumo
     * @return \Illuminate\Http\Response
     */
    public function update(InsumoFormRequest $request, Insumo $insumo)
    {
        $insumo->update($request->all());

        return redirect()->route('insumos.index')
            ->with('info', 'Insumo actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StopTime\Insumo  $insumo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insumo $insumo)
    {
        $insumo->delete();
        return back()->with('info', 'Eliminado correctamente');
    }
}
