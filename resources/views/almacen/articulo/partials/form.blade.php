<style>
	.lista  li > .azucar_crema > label:nth-child(2){
		order: 3;
	}
</style>

<div class="row">
	<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 center-block">
			<div class="row">
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">

					{{ Form::label('nombre', 'Nombre') }}
					{{ Form::text('nombre', null , ['class' => 'form-control' , 'placeholder' => 'Nombre']) }}

				</div>

				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">

					<label for = "categoria">Categoria</label>
					<select name="id_categoria" class="form-control">
						@foreach($categorias as $cat)
							<option value="{{$cat->id}}" style="text-transform: capitalize;" {{($cat->id == old('id_categoria')? 'selected':'')}} >{{$cat->nombre}}</option>
						@endforeach
					</select>
				</div>
			</div>

						



			<h4>Lista de insumos</h4>
			<div class="form-group lista ">
				<div class="row max-h-overflow-y">
					@foreach($insumos as $insumo)
				    	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
				    		<li>
						        <label> 

									<input type="checkbox" name="insumos[]" value="{{$insumo->id}}" class="chekbox-permisos checkbox_insumos" id="chekbox-permisos-{{$insumo->id}}" {{($insumo->marcado == true)? 'checked' :'' }}>

								     <label for="chekbox-permisos-{{$insumo->id}}" class="checkbox-personalizado"></label>
							        
									<div class="checkbox-personalizado-descripcion">
								        <label for="chekbox-permisos-{{$insumo->id}}" style="text-transform: capitalize;" >{{ $insumo->nombre }}</label>
							        </div>
						        </label>
						    </li>
				    	</div>
				    @endforeach
				</div>
			</div>


			<div class="row">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<div class="form-group">
						{{ Form::label('cant_insumos', 'Cant. insumos') }}
						{{ Form::number('cant_insumos', null , [ 'placeholder' => '1' , 'class' => 'form-control' , 'min' => '1', 'max' => '1', 'id' => 'cant_insumos']) }}
					</div>
				</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<div class="form-group">
						{{ Form::label('stock', 'Stock') }}
						{{ Form::number('stock', null , [ 'placeholder' => '0' , 'class' => 'form-control']) }}
					</div>
				</div>
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<div class="form-group">
						{{ Form::label('precio', 'Valor') }}
					${{ Form::number('precio', null , [ 'placeholder' => '0' , 'class' => 'form-control']) }}
					</div>
				</div>
			</div>

			<div class="row lista">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="form-group">
						<li>
						        <label class="azucar_crema"> 
					
									@if(!empty($articulo->crema))
										<input type="checkbox" name="crema" class="chekbox-permisos" value="1" id="crema" {{($articulo->crema == 1)? 'checked' :'' }}>
									@else
										<input type="checkbox" name="crema" class="chekbox-permisos" value="1" id="crema" >
									@endif
								     <label for="crema" class="checkbox-personalizado"></label>
							        
									<div class="checkbox-personalizado-descripcion">
								        <label for="crema" >Crema</label>
							        </div>
						        </label>
						    </li>
					</div>
				</div>
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="">
						<li>
						        <label class="azucar_crema"> 
						        	@if(!empty($articulo->azucar))
										<input type="checkbox" name="azucar" class="chekbox-permisos" value="1" id="azucar" {{($articulo->azucar == 1)? 'checked' :'' }}>
									@else
										<input type="checkbox" name="azucar" class="chekbox-permisos" value="1" id="azucar">
									@endif

								     <label for="azucar" class="checkbox-personalizado"></label>
							        
									<div class="checkbox-personalizado-descripcion">
								        <label for="azucar" >Azucar</label>
							        </div>
						        </label>
						    </li>
					</div>
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('descripcion', 'Descripción') }}
				{{ Form::textarea('descripcion', null, ['class' => 'form-control textarea-personalizado', 'placeholder' => 'Descrpcion', 'id' => 'max-descripcion-textarea' , 'maxlength' => '500']) }}
			</div>
			<div class="max-descripcion">
				<lavel>maximo de caracteres : <span id= "max-descripcion-numero">500</span> </lavel>
			</div>

		</div>



		<div class="image-editor col-lg-6 col-sm-12 col-md-6 col-xs-12" >
			<div class="image-editor2">
				@if(empty($articulo->imagen))
			    	<input id="imagen_imput"  onchange="habilitar_edicion_imagen2()" type="file" class="imput-personalizado cropit-image-input" value="{{ old('image-data') }}" name="imagen">
			    @else
			    	<input id="imagen_imput" type="file" class="imput-personalizado cropit-image-input" name="imagen" value="{{asset('imagenes/articulos/'.$articulo->imagen)}}">
			    @endif

			    

			    <label for="imagen_imput"  class="">
			    	<img src="{{asset('img/subir-imagen.png')}}">
			    </label>

			    <div class="cropit-responsive">
			      		<div class="sub-cropit-responsive">
			      			<div class="cropit-preview"></div>
			      		</div>
				</div>

				<div class="sub-titulo"> Tamaño minimo imagen 300x300 px</div>

				@if(empty($articulo->imagen))
			    	
				@else
					<div class="boton-imagen-panel btn btn-info" onclick="habilitar_edicion_imagen()">Editar imagen</div>		    	
					<input type="hidden" name="imagen_anterior" value="{{$articulo->imagen}}" >
				@endif
				

				<div class="imagen-panel">
				    <div class="image-size-label"></div>
				    
					<div class="rotate-section">
						<div class="rotate-ccw rotate-imagen"><img src="{{asset('img/giro-iz.png')}}" alt=""></div>
					    <div class="rotate-cw rotate-imagen"><img src="{{asset('img/giro-iz.png')}}" alt=""></div>
						<input id="range-rotate" type="range" class="cropit-image-zoom-input slider-range">
        		 		<input type="hidden" name="image-data" class="hidden-image-data" />
					    
				    </div>
				</div>
		    </div>
		</div>

		<!-- end jcrop -->
</div>



<div class="row" onLoad>
	<div class="col-lg-6 col-sm-8 col-md-12 col-xs-12 center-block">
			<div class="form-group botonera-form" >
				<button class="btn btn-info" type="submit">Guardar</button>
			</div>
	</div>
</div>


<script>

	window.addEventListener('load', function(){

		var articulosInsumos = {
			inputCantInsumos: document.querySelector('#cant_insumos'),
			checkbox: document.querySelectorAll('.checkbox_insumos'),
			checkbosChecked: null,
			run:()=>{
				for(var i = 0 ; i < articulosInsumos.checkbox.length ; i++){
					articulosInsumos.checkbox[i].addEventListener('change', articulosInsumos.cont , false);
				}
			},
			cont:()=>{
				articulosInsumos.checkbosChecked = document.querySelectorAll('.checkbox_insumos:checked').length
				if(articulosInsumos.checkbosChecked > 0){
					articulosInsumos.inputCantInsumos.max = articulosInsumos.checkbosChecked;
					articulosInsumos.inputCantInsumos.value = articulosInsumos.checkbosChecked;
					articulosInsumos.inputCantInsumos.readOnly = false;
				}else{
					articulosInsumos.inputCantInsumos.max = 1;
					articulosInsumos.inputCantInsumos.value = 0;
					articulosInsumos.inputCantInsumos.readOnly = true;
				}
			}
		};
		articulosInsumos.cont();
		articulosInsumos.run();

	} , false);
	
</script>